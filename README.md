# Image to ASCII

## Description

The "Image to ASCII" project is a Python application that transforms the video feed from your camera into a virtual camera, but with the output displayed in ASCII characters. It allows you to see the real world through the lens of ASCII art.

## Features

- Converts video feed to ASCII art.
- Adjustable compression level for rendering.
- Outputs the transformed image to a virtual camera.

## Usage

1. **Prerequisites**: Make sure you have the following Python libraries installed: numpy, cv2, PIL (Pillow), pyvirtualcam. You can install them using pip:
   
   ```
   pip install numpy opencv-python-headless Pillow pyvirtualcam
   ```

2. **Running the Application**: Execute the `main.py` script. You can use the following command-line argument to specify the compression factor (optional):

   ```
   python main.py -c 8 4
   ```

   The `-c` option allows you to adjust the compression factor with two integers representing the number of rows and columns to skip in the image. The default is set to `(8, 4)`.

3. **Virtual Camera**: The transformed ASCII image will be displayed in a virtual camera feed.