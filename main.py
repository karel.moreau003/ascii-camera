import numpy as np
from cv2 import VideoCapture
from PIL import Image, ImageDraw
import pyvirtualcam
import argparse


class Image_to_assci:
    def grey_scale(self):
        self.img = np.average(self.img, axis=2, weights=[0.2126, 0.7152, 0.0722])

    def compress(self, facteurCompression):
        iy = range(0, self.img.shape[0], facteurCompression[0])

        ix = range(0, self.img.shape[1], facteurCompression[1])
        self.img = self.img[np.ix_(iy, ix)]

    def img_to_ascci(self, characteres):
        i = np.array(self.img / 255 * len(characteres), dtype=int)
        self.asciiImg = characteres[i]
        self.asciiImg = "\n".join(["".join(ligne) for ligne in self.asciiImg])

    def ascii_to_png(self):
        d = ImageDraw.Draw(self.pngImg)
        for i, ligne in enumerate(self.asciiImg.split("\n")):
            d.text((0, i * 8), ligne, fill=(0, 0, 0))

    def __init__(self, cam, pngImg):
        result, image = cam.read()
        if result:
            self.img = np.array(image)
            self.asciiImg = None
            self.pngImg = pngImg
        else:
            raise "Failed to capture image"



character = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\|()1{}[]?-_+~<>i!lI;:,\"^`'. "
character = np.array(list(character))

cam = VideoCapture(0)
parser = argparse.ArgumentParser(description='A test program.')
parser.add_argument("-c", "--compression", help="Facteur de compression du rendu", type=int, nargs=2, default=(8, 4))
args = parser.parse_args()
facteurCompression = args.compression
pngImg = Image.new('RGB', (int(640 / facteurCompression[1] * 6), int(480 / facteurCompression[0] * 8)),
                   color=(255, 255, 255))
i = 0
with pyvirtualcam.Camera(width=1280, height=720, fps=60) as vcam:
    while True:
        img = Image_to_assci(cam, pngImg)
        img.grey_scale()
        img.compress(facteurCompression)
        img.img_to_ascci(character)
        img.ascii_to_png()
        img.pngImg = img.pngImg.resize((1280, 720), 1)
        ImageDraw.Draw(pngImg).rectangle((0, 0, 3840, 1922), 'white')
        vcam.send(np.array(img.pngImg))
